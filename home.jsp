<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>TC to SAP Home</title>
	<script src="js/jquery.min.js"></script>
	<script src="js/kendo.all.min.js"></script>
	<script src="js/common.js"></script>
	<script src="js/home.js"></script>
	<link href="css/kendo.default-v2.min.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="all">
	<link href="css/fonts.css" type="text/css"rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/general.css" media="all">
	<jsp:include page="sucessFailureScript.jsp"></jsp:include>
</head>
<body >

<div>
		<jsp:include page="header.jsp"></jsp:include>
	</div>
	<!-- Body -->
	<form name="home" id="home" method="post">
		<section id="services">
			<div class="container">
				<div class="contentPageTop text-center our-services">
					<div class="row">
						<div class="col-sm-offset-1 col-sm-5">
							<div class="service-icon">

								<a onclick="javascript:navigate('folderPaths');"> <i id="icon"
									class="fa service-icon eQ-fonts-start_debug_hover"></i></a>
							</div>
							<div class="service-info ">
								<h3 class="priMenuResearch">
									<a onclick="javascript:navigate('folderPaths');">Migration</a>
								</h3>
								<p>Navigate to the Extracted Folder Paths Page</p>

							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
	</form>
	<p id="message" style="display:none;"><%= session.getAttribute("message")%></p>
</body>
</html>
