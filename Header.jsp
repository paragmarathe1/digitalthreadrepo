
<nav class="navbar navbar-default navbar-fixed-top topnav"
	role="navigation">
	<div class="container-fluid topnav">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<form name="logo" id="logo" method="post">
			<div id="RRlogo">
				<a onclick="javascript:navigateHome()"><img class="img-responsive"
					src="images/RRLogo1.png"></a>
			</div>
			<div class="headerTitle">Teamcenter To SAP Material Master Transfer</div>
		</form>

		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		 <form name="header" id="logout" method="post">
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul  class="nav navbar-nav navbar-right">
				<%
				out.print( "<li><span class='headerTitle'> Welcome " +  session.getAttribute("username") + "</span></li>");
				%>
				<!--  <li><span><a  onclick="javascript:navigateHome()"><i class="eQ-fonts-homeActive"></i></a></span></li>
				<li><span><i data-toggle="tooltip"   title="kendo.js 2015.1.5.11"  id="about" class="eQ-fonts-information"></i></span></li>
				<li><a onclick="javascript:Logout()" style="font-size:20px;padding-left:10px;" >Logout</a></li>
				<li><a href="javascript:Logout()" style="font-size:20px;padding-left:10px;" >Logout</a></li> -->
			</ul>
		</div>
		</form>
	</div>
</nav>

<script>

function Logout()
{
	showOverlay();
	 document.header.action = "TC2SAPController?tc2sapaction=logout";
	 document.header.submit();
}
function navigateHome()
{
	showOverlay();
	document.header.action = "TC2SAPController?tc2sapaction=showHome";
	document.header.submit();
}
</script>
